import requests
from abc import ABCMeta, abstractmethod
from flask import url_for, redirect, request, session
from flask.ext.login import (
    LoginManager,
    login_required as saml_login_required,
    login_user as saml_login_user,
    logout_user as saml_logout_user,
    current_user as saml_current_user,
)
from flask_cas import (
    CAS,
    login_required as cas_login_required,
    logout as cas_logout
)
from piano_users.user_dao import User, AnonymousUser, user_dao
from saml2 import BINDING_HTTP_POST, BINDING_HTTP_REDIRECT, entity
from saml2.client import Saml2Client
from saml2.config import Config as Saml2Config

PIANO_AUTH_REQUIRED_CONFIGS = ["PIANO_AUTH_TYPE", "PIANO_AUTH_IDP_URLS"]
PIANO_AUTH_TYPES = ["CAS", "SAML"]


class PianoAuthConfigException(Exception):
    def __init__(self, message="An error occurred trying to access required PianoAuth config value. "
                               "The following configs are required: %s" % ", ".join(PIANO_AUTH_REQUIRED_CONFIGS)):
        self.message = message


class PianoAuth:
    __metaclass__ = ABCMeta

    idp_urls = {}
    after_login_url = "index"
    after_logout_url = None

    user_first_name_field = "firstname"
    user_last_name_field = "lastname"
    user_email_field = "email"
    user_admin_field = "Admin"

    user_dao = None

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        if "PIANO_AUTH_AFTER_LOGIN" in app.config:
            self.after_login_url = app.config["PIANO_AUTH_AFTER_LOGIN"]

        if "PIANO_AUTH_AFTER_LOGOUT" in app.config:
            self.after_logout_url = app.config["PIANO_AUTH_AFTER_LOGOUT"]

        if "PIANO_AUTH_IDP_URLS" in app.config:
            self.idp_urls = app.config["PIANO_AUTH_IDP_URLS"]
        if idp_urls:
            self.idp_urls = idp_urls

        if not self.idp_urls:
            raise PianoAuthConfigException("PIANO_AUTH_IDP_URLS is not set")

        if "PIANO_AUTH_USER_API" in app.config and app.config["PIANO_AUTH_USER_API"]:
            url = app.config.get("PIANO_AUTH_USER_API", None)
            self.user_dao = user_dao(
                dao_type="api",
                api_url=url
            )
        elif "PIANO_AUTH_USER_DB_HOST" in app.config and "PIANO_AUTH_USER_DB_PORT" in app.config \
                and "PIANO_AUTH_USER_DB_NAME" in app.config:
            self.user_dao = user_dao(
                dao_type=app.config.get("PIANO_AUTH_USER_DB_TYPE", "mongodb"),
                host=app.config["PIANO_AUTH_USER_DB_HOST"],
                port=app.config["PIANO_AUTH_USER_DB_PORT"],
                database=app.config.get("PIANO_AUTH_USER_DB_NAME")
            )

    @abstractmethod
    def login_required(self, func):
        pass

    @abstractmethod
    def current_user(self):
        pass

    @abstractmethod
    def logout(self):
        pass

    @abstractmethod
    def admin_user(self):
        pass

    @abstractmethod
    def user_id(self):
        pass

    @abstractmethod
    def user_email(self):
        pass

    @abstractmethod
    def authenticate_anonymous_user(self, admin=False):
        pass

    def user_roles(self):
        return []


class PianoCAS(PianoAuth, CAS):

    # TODO fix integration with user_dao

    user = None

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        PianoAuth.__init__(self, app, idp_urls)

        app.config.update(
            CAS_SERVER=self.idp_urls.values()[0],
            CAS_VALIDATE_ROUTE='/cas/p3/serviceValidate',
            CAS_AFTER_LOGIN=self.after_login_url
        )
        if self.after_logout_url:
            app.config.update(
                CAS_AFTER_LOGOUT=self.after_logout_url,
            )

        CAS.__init__(self, app, '/cas')

    def current_user(self):
        user_id = self.user_id()
        if not self.user:
            user = {
                "email": self.user_email(),
                "roles": self.user_roles()
            }
            self.user = User(self.user_dao, user_id, user)
        return self.user

    def user_id(self):
        user_id = None
        if self.attributes:
            user_id = self.attributes.get("cas:_id", None)
        if isinstance(user_id, basestring):
            return user_id.lower()
        return user_id

    def user_email(self):
        email = None
        if 'CAS_USERNAME' in session:
            email = session.get("CAS_USERNAME")
            email = email.replace("MongoProfile#", "")
        return email

    def authenticate_anonymous_user(self, admin=False):
        session.update({
            "CAS_USERNAME": "anonymous"
        })
        if admin:
            session.update({"cas:roles": ["admin"]})

    def admin_user(self):
        admin = False
        if self.token or session.get("CAS_USERNAME") == "anonymous":
            if "admin" in self.user_roles():
                admin = True
        return admin

    def login_required(self, func):
        return cas_login_required(func)

    def logout(self):
        for k in session.keys():
            del session[k]
            # if k.startswith("CAS") or k.startswith("_CAS"):
            #     session.pop(k)
        return cas_logout()

    def user_roles(self):
        roles = []
        if self.attributes:
            roles = self.attributes.get("cas:roles", [])
            if type(roles) is not list:
                roles = [roles]
        if session.get("CAS_USERNAME") == "anonymous":
            anon_roles = session.get("cas:roles", [])
            if anon_roles:
                roles += anon_roles
        return roles


class PianoSAML(PianoAuth, LoginManager):

    # SAML/Okta integration code taken and adapted from https://github.com/jpf/okta-pysaml2-example.git

    saml_metadata = {}

    def __init__(self, app, idp_urls=None, *args, **kwargs):
        PianoAuth.__init__(self, app, idp_urls)
        LoginManager.__init__(self)
        self.init_app(app)

        if "PIANO_AUTH_SAML_METADATA" in app.config:
            self.saml_metadata.update(app.config["PIANO_AUTH_SAML_METADATA"])

        @self.user_loader
        def load_user(user_id):
            return User(self.user_dao, user_id)

        @self.unauthorized_handler
        def unauthorized():
            return redirect(url_for("piano_login", idp_name=self.idp_urls.keys()[0]))

        @app.route("/saml/sso/<idp_name>", methods=['POST'])
        def piano_sso_login(idp_name):
            return self.idp_login(idp_name)

        @app.route("/saml/login/<idp_name>")
        def piano_login(idp_name):
            return self.service_provider_login(idp_name)

        self.anonymous_user = AnonymousUser

    def current_user(self):
        return saml_current_user

    def user_id(self):
        user_id = session.get("uid")
        if not user_id:
            user_id = session.get("user_id")
        if not user_id:
            user_id = self.user_email()
        if isinstance(user_id, list):
            user_id = user_id[0]
        if isinstance(user_id, basestring):
            return user_id.lower()
        return user_id

    def user_email(self):
        return session.get(self.user_email_field)

    def authenticate_anonymous_user(self, admin=False):
        self.current_user().__setattr__("authenticated", True)
        if admin:
            self.current_user().__setattr__("roles", ["admin"])

    def admin_user(self):
        return "admin" in saml_current_user.roles

    def login_required(self, func):
        return saml_login_required(func)

    def logout(self):
        return saml_logout_user()

    def saml_client_for(self, idp_name=None):
        """
        Given the name of an IdP, return a configuration.
        The configuration is a hash for use by saml2.config.Config
        """

        if idp_name not in self.idp_urls:
            raise Exception("Settings for IDP '{}' not found".format(idp_name))
        acs_url = url_for(
            "piano_sso_login",
            idp_name=idp_name,
            _external=True)
        https_acs_url = url_for(
            "piano_sso_login",
            idp_name=idp_name,
            _external=True,
            _scheme='https')

        if self.saml_metadata and idp_name in self.saml_metadata:
            metadata = self.saml_metadata[idp_name]
        else:
            #   SAML metadata changes very rarely. On a production system,
            #   this data should be cached as appropriate for your production system.
            rv = requests.get(self.idp_urls[idp_name])
            metadata = rv.text

        settings = {
            'metadata': {
                'inline': [metadata],
            },
            'service': {
                'sp': {
                    'endpoints': {
                        'assertion_consumer_service': [
                            (acs_url, BINDING_HTTP_REDIRECT),
                            (acs_url, BINDING_HTTP_POST),
                            (https_acs_url, BINDING_HTTP_REDIRECT),
                            (https_acs_url, BINDING_HTTP_POST)
                        ],
                    },
                    # Don't verify that the incoming requests originate from us via
                    # the built-in cache for authn request ids in pysaml2
                    'allow_unsolicited': True,
                    # Don't sign authn requests, since signed requests only make
                    # sense in a situation where you control both the SP and IdP
                    'authn_requests_signed': False,
                    'logout_requests_signed': True,
                    'want_assertions_signed': True,
                    'want_response_signed': False,
                },
            },
        }
        spConfig = Saml2Config()
        spConfig.load(settings)
        spConfig.allow_unknown_attributes = True
        saml_client = Saml2Client(config=spConfig)
        return saml_client

    def idp_login(self, idp_name):
        saml_client = self.saml_client_for(idp_name)
        authn_response = saml_client.parse_authn_request_response(
            request.form['SAMLResponse'],
            entity.BINDING_HTTP_POST)
        authn_response.get_identity()
        user_info = authn_response.get_subject()
        username = user_info.text

        # This is what as known as "Just In Time (JIT) provisioning".
        # What that means is that, if a user in a SAML assertion
        # isn't in the user store, we create that user first, then log them in
        user_attributes = {
            'first_name': authn_response.ava.get(self.user_first_name_field, [None])[0],
            'last_name': authn_response.ava.get(self.user_last_name_field, [None])[0],
            'email': authn_response.ava.get(self.user_email_field, [None])[0],
            'roles': []
        }
        if authn_response.ava.get(self.user_admin_field, [None])[0] == "true":
            user_attributes['roles'] += ["admin"]
        if "uid" in authn_response.ava and authn_response.ava["uid"]:
            username = authn_response.ava["uid"]
            if isinstance(username, list):
                username = username[0]
        user = User(self.user_dao, username, user_attributes)
        session['saml_attributes'] = authn_response.ava
        saml_login_user(user)
        url = url_for(self.after_login_url)
        # NOTE:
        #   On a production system, the RelayState MUST be checked
        #   to make sure it doesn't contain dangerous URLs!
        # TODO check relay state url
        if 'RelayState' in request.form:
            url = request.form['RelayState']
        return redirect(url)

    def service_provider_login(self, idp_name):
        saml_client = self.saml_client_for(idp_name)
        reqid, info = saml_client.prepare_for_authenticate()

        redirect_url = None
        # Select the IdP URL to send the AuthN request to
        for key, value in info['headers']:
            if key is 'Location':
                redirect_url = value
        response = redirect(redirect_url, code=302)
        # NOTE:
        #   I realize I _technically_ don't need to set Cache-Control or Pragma:
        #     http://stackoverflow.com/a/5494469
        #   However, Section 3.2.3.2 of the SAML spec suggests they are set:
        #     http://docs.oasis-open.org/security/saml/v2.0/saml-bindings-2.0-os.pdf
        #   We set those headers here as a "belt and suspenders" approach,
        #   since enterprise environments don't always conform to RFCs
        response.headers['Cache-Control'] = 'no-cache, no-store'
        response.headers['Pragma'] = 'no-cache'
        return response


def piano_authentication(app, auth_type=None, *args, **kwargs):
    if auth_type is None and "PIANO_AUTH_TYPE" in app.config:
        auth_type = app.config["PIANO_AUTH_TYPE"]

    if auth_type == "CAS":
        return PianoCAS(app, *args, **kwargs)
    elif auth_type == "SAML":
        return PianoSAML(app, *args, **kwargs)
    else:
        raise PianoAuthConfigException("Invalid PIANO_AUTH_TYPE. Must be one of %s" % ", ".join(PIANO_AUTH_TYPES))
