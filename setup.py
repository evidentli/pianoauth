from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='piano_auth',
    packages=find_packages(exclude=["tests"]),
    version='0.13.0',
    description='Piano Auth',
    author='Michael Van Treeck',
    author_email='michael@evidentli.com',
    url='https://bitbucket.org/evidentli/pianoauth',
    license='MIT',
    keywords=['piano', 'util', 'auth', 'cas', 'saml', 'okta'],
    classifiers=[],
    install_requires=requirements,
)
